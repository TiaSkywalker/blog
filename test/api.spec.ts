import { expect } from "chai";
import { DynamoDB } from "aws-sdk";
import * as uuid from "uuid/v4";
import { server } from "../src/server";

describe("/posts", function () {
    this.timeout(5000);
    before("setup api", async function () {
        await server.ready();
    });
    before("clear tables", async function () {
    });
    it("POST /posts", async () => {
        const payload = {
            "title": "My real first post",
            "content": "Lorem ipsum dolor sit amet"
        };
        const expected = {
            "id": 1,
            "title": payload.title,
            "timestamp": "2018-11-23T12:36:01.674Z",
            "content": payload.content,
        };
        const response = await server.inject({
            url: "/posts",
            method: "POST",
            headers: {
                authorization: "Token token"
            },
            payload: payload,
        });
        expect(response.statusCode).to.equal(201);
        const body = JSON.parse(response.payload);
        console.log(body);
        expect(body).to.have.property("id").to.be.a("number");
        expect(body).to.have.property("title").to.be.equal(expected.title);
        expect(body).to.have.property("timestamp").to.be.a("string");
        expect(body).to.have.property("content").to.be.equal(expected.content);
    });
    it("PATCH /posts/:id", async () => {
        const payload = {
            "title": "My First Modified Post",
            "content": "Modified lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla velit ante, facilisis non iaculis ut, viverra sed ex. Nam molestie felis nec volutpat tristique. Mauris id facilisis odio. Donec pharetra tincidunt velit sed hendrerit. Morbi sed velit quis augue imperdiet malesuada. Integer dapibus urna purus, ac mattis urna sollicitudin rutrum. Vestibulum mollis, tellus nec ullamcorper lacinia, ligula augue vestibulum neque, nec ornare ex ex eu erat. Quisque in odio odio. Vivamus dignissim turpis sit amet felis ultricies faucibus. Nunc ante eros, vestibulum tincidunt imperdiet non, convallis nec mi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam fermentum ligula vel ante consectetur, sed faucibus lacus tempor. Nullam pharetra, velit eu sollicitudin auctor, nulla enim convallis turpis, condimentum consequat sapien sem vitae tellus. Aliquam tincidunt erat at tincidunt sagittis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        };
        const expected = {
            "title": payload.title,
            "content": payload.content
        };
        const response = await server.inject({
            url: "/posts/4",
            method: "PATCH",
            headers: {
                authorization: "Token token"
            },
            payload: payload,
        });
        expect(response.statusCode).to.equal(200);
        const body = JSON.parse(response.payload);
        expect(body).to.deep.equal(expected);
    });
    it("DELETE /posts/:id", async () => {
        const payload = {};
        const expected = {};
        const response = await server.inject({
            url: "/posts/6",
            method: "DELETE",
            headers: {
                authorization: "Token token"
            },
            payload: payload,
        });
        expect(response.statusCode).to.equal(200);
        const body = JSON.parse(response.payload);
        expect(body).to.be.deep.equal(expected);
    });
    it("GET /posts", async () => {
        const payload = {};
        const response = await server.inject({
            url: "/posts",
            method: "GET",
            payload: payload,
        });
        expect(response.statusCode).to.equal(200);
        const body = JSON.parse(response.payload);
        expect(body).to.be.a("array");
    });
    it("GET /posts/:title/:content/:username", async () => {
        const payload = {};
        const response = await server.inject({
            url: "/posts/post/lorem/Mattia",
            method: "GET",
            payload: payload,
        });
        expect(response.statusCode).to.equal(200);
        const body = JSON.parse(response.payload);
        expect(body).to.be.a("array");
    });
    it("GET /posts/:id", async () => {
        const payload = {};
        const expected = {
            "id": 3,
            "title": "Monday Post",
            "authorUsername": "Mattia",
            "authorId": 2,
            "timestamp": "2018-12-10T08:28:12.598Z",
            "content": "Lorem ipsum dolor sit amet",
        };
        const response = await server.inject({
            url: "/posts/3",
            method: "GET",
            headers: {},
            payload: payload,
        });
        expect(response.statusCode).to.equal(200);
        const body = JSON.parse(response.payload);
        expect(body).to.have.property("id").to.be.a("number");
        expect(body).to.have.property("title").to.be.a("string");
        expect(body).to.have.property("authorUsername").to.be.a("string");
        expect(body).to.have.property("timestamp").to.be.a("string");
        expect(body).to.have.property("content").to.be.a("string");
    });
    it("POST /favorite", async () => {
        const payload = {
            "userId": 1,
            "postId": 20,
        };
        const expected = {
            "message": "Favorite added",
        };
        const response = await server.inject({
            url: "/favorite",
            method: "POST",
            headers: {
                authorization: "Token token"
            },
            payload: payload,
        });
        expect(response.statusCode).to.equal(201);
        const body = JSON.parse(response.payload);
        console.log(body);
        expect(body).to.have.property("message").to.be.a("string");
    });
    it("GET /favorite/:userId/:postId", async () => {
        const payload = {};
        const expected = {
            "flag": "true"
        };
        const response = await server.inject({
            url: "/favorite/2/2",
            method: "GET",
            headers: {
                authorization: "Token token"
            },
            payload: payload,
        });
        expect(response.statusCode).to.equal(200);
        const body = JSON.parse(response.payload);
        expect(body).to.have.property("flag").to.be.a("boolean");
    });
    it("GET /favorites/:userId", async () => {
        const payload = {};
        const response = await server.inject({
            url: "/favorites/2",
            method: "GET",
            headers: {
                authorization: "Token token"
            },
            payload: payload,
        });
        expect(response.statusCode).to.equal(200);
        const body = JSON.parse(response.payload);
        expect(body).to.a("array");
    });
});

describe("/authors", function () {
    this.timeout(5000);
    before("setup api", async function () {
        await server.ready();
    });
    before("clear tables", async function () {
    });
    it("GET /authors", async () => {
        const payload = {};
        const response = await server.inject({
            url: "/authors",
            method: "GET",
            headers: {
                authorization: "Token token"
            },
            payload: payload,
        });
        expect(response.statusCode).to.equal(200);
        const body = JSON.parse(response.payload);
        expect(body).to.be.a("array");
    });
    it("GET /authors/:id", async () => {
        const payload = {};
        const expected = {
            "id": 1,
            "username": "Admin",
        };
        const response = await server.inject({
            url: "/authors/1",
            method: "GET",
            headers: {
                authorization: "Token token"
            },
            payload: payload,
        });
        expect(response.statusCode).to.equal(200);
        const body = JSON.parse(response.payload);
        expect(body.id).to.be.a("number");
        expect(body.username).to.be.a("string");
    });
    it("POST /REGISTER", async () => {
        const payload = {
            "username": uuid(),
            "password": "tester1"
        };
        const expected = {};
        const response = await server.inject({
            url: "/register",
            method: "POST",
            headers: {},
            payload: payload,
        });
        expect(response.statusCode).to.equal(201);
        const body = JSON.parse(response.payload);
        expect(body).to.be.deep.equal(expected);
    });
    it("POST /login", async () => {
        const payload = {
            "username": "Mattia",
            "password": "mattia1"
        };
        const expected = {
            "token": "token",
        };
        const response = await server.inject({
            url: "/login",
            method: "POST",
            headers: {},
            payload: payload,
        });
        expect(response.statusCode).to.equal(200);
        const body = JSON.parse(response.payload);
        expect(body).to.have.property("token").to.be.a("string");
    });
});
