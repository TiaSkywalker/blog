export type Author = {
    id: number;
    username: string;
    password: string;
    token?: string;
};

export type Post = {
    id: number,
    title: string,
    authorId: number,
    timestamp: string,
    content: string,
    favorite: boolean,
};
