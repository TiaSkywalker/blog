import * as fastify from "fastify";
import { DynamoDB } from "aws-sdk";
import * as uuid from "uuid/v4";
import { Author, Post } from "./types";
import * as bcrypt from "bcrypt";

const ddb = new DynamoDB.DocumentClient({
    region: "us-west-2",
});
export const server = fastify({
    logger: true,
});
server.register(require("fastify-cors"), {});

const authHandler = async (request, reply) => {
    const resAuthors = await ddb.scan({
        TableName: "Mattia_Blog_AuthorsDB",
    }).promise();
    const auth = request.headers["authorization"] as string;
    if (!auth) {
        reply.code(401);
        throw new Error();
    }
    const s = auth.split(" ");
    if (s[0] !== "Token") {
        reply.code(401);
        throw new Error();
    }
    const author = resAuthors.Items.find((a) => a.token === s[1]);
    if (!author) {
        reply.code(401);
        throw new Error();
    }
    request["author"] = author as Author;
    return;
};

server.route({ // POST /register
    method: "POST",
    url: "/register",
    schema: {
        body: {
            type: "object",
            properties: {
               username: {
                    type: "string",
                    minLenght: 1,
                },
                password: {
                    type: "string",
                    minLenght: 4,
                    pattern: "^[\\d\\S]*\\d[\\d+\\S]*$",
                },
            },
            required: [ "username", "password" ]
        }
    },
    handler: async (request, reply) => {
        const parsedRegister = request.body as { username: string, password: string };
        const resAuthors = await ddb.scan({
            TableName: "Mattia_Blog_AuthorsDB",
        }).promise();
        const authorTaken = resAuthors.Items.find((user) => user.username === parsedRegister.username);
        if ( authorTaken ) {
            reply.code(400);
            return { message: "Username " + parsedRegister.username + " is already in use" };
        } else {
            const authorId = resAuthors.Items.reduce((aId, user) => aId < user.id ? user.id : aId, 0) + 1;
            const hash = await bcrypt.hash(parsedRegister.password, 10);
            await ddb.put({
                TableName: "Mattia_Blog_AuthorsDB",
                Item: {
                    id: authorId,
                    username: parsedRegister.username,
                    password: hash,
                }
            }).promise();
            reply.code(201);
            return {};
        }
    }
});
server.route({ // POST /login
    method: "POST",
    url: "/login",
    schema: {
        body: {
            type: "object",
            properties: {
               username: {
                    type: "string",
                },
                password: {
                    type: "string",
                },
            },
            required: [ "username", "password" ]
        }
    },
    handler: async (request, reply) => {
        const parsedLogin = request.body as { username: string, password: string };
        const user = await ddb.scan({
            TableName: "Mattia_Blog_AuthorsDB",
            IndexName: "username-index",
            FilterExpression: "username = :username",
            ExpressionAttributeValues: {
                ":username": parsedLogin.username,
            }
        }).promise();
        if ( user.Count === 0 ) {
            reply.code(400);
            return{ message: "Invalid username or password" };
        }
        if ( !(await bcrypt.compare(parsedLogin.password, user.Items[0].password)) ) {
            reply.code(400);
            return{ message: "Invalid username or password" };
        }
        const token = uuid();
        await ddb.update({
            TableName: "Mattia_Blog_AuthorsDB",
            Key: { id: user.Items[0].id },
            AttributeUpdates: {
                token: {
                    Action: "PUT",
                    Value: token,
                }
            }
        }).promise();
        return {
            token: token,
            id: user.Items[0].id,
        };
    }
});
server.route({ // GET /authors
    method: "GET",
    url: "/authors",
    config: {
        auth: true,
    },
    schema: {},
    beforeHandler: authHandler,
    handler: async (request, reply) => {
        const resAuthors = await ddb.scan({
            TableName: "Mattia_Blog_AuthorsDB",
        }).promise();
        const returnedAuthors = resAuthors.Items.map((a) => {
            let author;
            if (a) {
                author = {
                    id: a.id,
                    username: a.username,
                };
            }
            return author;
        });
        return returnedAuthors;
    }
});
server.route({ // GET /authors/:id
    method: "GET",
    url: "/authors/:id",
    config: {
        auth: true,
    },
    schema: {
        params: {
            id: {
                type: "number"
            }
        },
    },
    beforeHandler: authHandler,
    handler: async (request, reply) => {
        const parsedId = request.params.id as number;
        const author = await ddb.get({
            TableName: "Mattia_Blog_AuthorsDB",
            Key: {
                id: parsedId,
            }
        }).promise();
        if (!author.Item) {
            reply.code(404);
            return { message: "Author not found" };
        } else {
            return {
                id: author.Item.id,
                username: author.Item.username
            };
        }
    }
});

server.route({ // POST /posts
    method: "POST",
    url: "/posts",
    config: {
        auth: true,
    },
    schema: {
        body: {
            type: "object",
            properties: {
               title: {
                    type: "string",
                    minLenght: 1,
                },
                content: {
                    type: "string",
                    minLenght: 1,
                },
            },
            required: [ "title", "content" ]
        }
    },
    beforeHandler: authHandler,
    handler: async (request, reply) => {
        const resPosts = await ddb.scan({
            TableName: "Mattia_Blog_PostsDB",
        }).promise();
        const author = request["author"] as Author;
        const parsedPost = request.body as { title: string, content: string };
        const postId = resPosts.Items.reduce((pId, post) => pId < post.id ? post.id : pId, 0) + 1;
        const timestamp = new Date().toISOString();
        await ddb.put({
            TableName: "Mattia_Blog_PostsDB",
            Item: {
                id: postId,
                title: parsedPost.title,
                authorId: author.id,
                timestamp: timestamp,
                content: parsedPost.content,
            },
        }).promise();
        reply.code(201);
        return {
            id: postId,
            title: parsedPost.title,
            timestamp: timestamp,
            content: parsedPost.content,
        };
    }
});
server.route({ // PATCH /posts/:id
    method: "PATCH",
    url: "/posts/:id",
    schema: {
        params: {
            id: {
                type: "number"
            }
        },
        body: {
            type: "object",
            properties: {
                title: {
                    type: "string",
                    minLenght: 10,
                },
                content: {
                    type: "string",
                    minLenght: 10,
                },
                favorite: {
                    type: "boolean",
                }
            },
        }
    },
    beforeHandler: authHandler,
    handler: async (request, reply) => {
        const parsedId = request.params.id as number;
        const parsedMods = request.body as { title: string, content: string, favorite: boolean };
        const post = await ddb.get({
            TableName: "Mattia_Blog_PostsDB",
            Key: {
                id: parsedId,
            }
        }).promise();
        if (!post.Item) {
            reply.code(404);
            return { message: "Post not found" };
        } else {
            const author = request["author"] as Author;
            const toPatch = post.Item;
            if (parsedMods.favorite !== undefined) {
                if (parsedMods.favorite === true) {
                    await ddb.put({
                        TableName: "Mattia_Blog_FavoriteDB",
                        Item: {
                            userId: author.id,
                            postId: toPatch.id,
                        }
                    }).promise();
                }
                else {
                    await ddb.delete({
                        TableName: "Mattia_Blog_FavoriteDB",
                        Key: {
                            userId: author.id,
                            postId: toPatch.id,
                        }
                    }).promise();
                }
            }
            if (parsedMods.title && toPatch.authorId === author.id) {
                await ddb.update({
                    TableName: "Mattia_Blog_PostsDB",
                    Key: {
                        id: parsedId
                    },
                    AttributeUpdates: {
                        title: {
                            Action: "PUT",
                            Value: parsedMods.title,
                        },
                    }
                }).promise();
            }
            if (parsedMods.content && toPatch.authorId === author.id) {
                await ddb.update({
                    TableName: "Mattia_Blog_PostsDB",
                    Key: {
                        id: parsedId
                    },
                    AttributeUpdates: {
                        content: {
                            Action: "PUT",
                            Value: parsedMods.content,
                        }
                    }
                }).promise();
            }
            const patchedPost = await ddb.get({
                TableName: "Mattia_Blog_PostsDB",
                Key: {
                    id: parsedId,
                }
            }).promise();
            return {
                title: patchedPost.Item.title,
                content: patchedPost.Item.content,
            };
        }
    }
});
server.route({ // DELETE /posts/:id
    method: "DELETE",
    url: "/posts/:id",
    config: {
        auth: true,
    },
    schema: {
        params: {
            id: {
                type: "number"
            }
        },
    },
    beforeHandler: authHandler,
    handler: async (request, reply) => {
        const parsedId = request.params.id as number;
        const post = await ddb.get({
            TableName: "Mattia_Blog_PostsDB",
            Key: {
                id: parsedId,
            },
        }).promise();
        if (!post.Item) {
            reply.code(404);
            return { message: "Post not found" };
        }
        const author = request["author"] as Author;
        const toDelete = post.Item;
        if (toDelete.authorId !== author.id ) {
            reply.code(403);
            return { message: "Forbidden" };
        }
        await ddb.delete({
            TableName: "Mattia_Blog_PostsDB",
            Key: {
                id: parsedId
            }
        }).promise();
        return { message: "Post deleted"};
    }
});
server.route({ // GET /posts
    method: "GET",
    url: "/posts",
    beforeHandler: async (request, reply) => {
        try {
            await authHandler(request, {
                code: () => {},
            });
        } catch (error) {
        }
    },
    handler: async (request, reply) => {
        const paramAuthors: DynamoDB.DocumentClient.ScanInput = {
            TableName: "Mattia_Blog_AuthorsDB"
        };
        if (request.query.username) {
            paramAuthors.FilterExpression = "username = :username";
            paramAuthors.ExpressionAttributeValues = {
                ":username": request.query.username,
            };
        }
        const resAuthors = await ddb.scan(paramAuthors).promise();
        if (resAuthors.Count === 0) {
            return [];
        }
        const paramPosts: DynamoDB.DocumentClient.ScanInput = {
            TableName: "Mattia_Blog_PostsDB"
        };
        if (request.query.username) {
            paramPosts.FilterExpression = "authorId = :authorId";
            paramPosts.ExpressionAttributeValues = {
                ":authorId": resAuthors.Items[0].id,
            };
        }
        let favorites = [];
        const author = request["author"] as Author;
        if (author) {
            const fav = await ddb.scan({
                TableName: "Mattia_Blog_FavoriteDB",
                IndexName: "postId-userId-index",
                FilterExpression: "userId = :userId",
                ExpressionAttributeValues: {
                    ":userId": author.id,
                }
            }).promise();
            if (fav.Count !== 0) {
                favorites = fav.Items;
            }
        }
        const resPosts = await ddb.scan(paramPosts).promise();
        let returnedPosts = resPosts.Items.map((p) => {
            const author = resAuthors.Items.find((a) => a.id === p.authorId);
            const a = { username: "" };
            if (author) {
                a.username = author.username;
            }
            const post = {
                id: p.id,
                title: p.title,
                authorId: p.authorId,
                authorUsername: a.username,
                timestamp: p.timestamp,
                content: p.content,
                favorite: favorites.some((fav) => fav.postId === p.id),
            };
            return post;
        });
        if (request.query.favorite !== undefined) {
            returnedPosts = returnedPosts.filter((post) => {
                return post.favorite === (request.query.favorite === "true");
            });
        }
        if (request.query.title) {
            returnedPosts = returnedPosts.filter((post) => {
                return post.title.toLowerCase().includes(request.query.title.toLowerCase());
            });
        }
        if (request.query.content) {
            returnedPosts = returnedPosts.filter((post) => {
                return post.content.toLowerCase().includes(request.query.content.toLowerCase());
            });
        }
        returnedPosts.sort(function (a, b) {
            return b.timestamp.localeCompare(a.timestamp);
        });
        return returnedPosts;
    }
});
server.route({ // GET /posts/:id
    method: "GET",
    url: "/posts/:id",
    schema: {
        params: {
            id: {
                type: "number"
            }
        },
    },
    handler: async (request, reply) => {
        const parsedId = request.params.id as number;
        const post = await ddb.get({
            TableName: "Mattia_Blog_PostsDB",
            Key: {
                id: parsedId,
            }
        }).promise();
        if (!post.Item) {
            reply.code(404);
            return { "message": "Post not found"};
        } else {
            const author = await ddb.get({
                TableName: "Mattia_Blog_AuthorsDB",
                Key: {
                    id: post.Item.authorId,
                },
            }).promise();
            const a = { username: "" };
            if (author) {
                a.username = author.Item.username;
            }
            return {
                id: post.Item.id,
                title: post.Item.title,
                authorId: post.Item.authorId,
                authorUsername: a.username,
                timestamp: post.Item.timestamp,
                content: post.Item.content,
            };
        }
    }
});
