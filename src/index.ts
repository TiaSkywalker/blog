import { server } from "./server";

const main = async () => {
    await server.listen(8000);
    return;
};

main();